package ru.yandex.market.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class ItemsListPageMarket {
    private WebDriver webDriver;
    private WebDriverWait wait;

    @FindBy(xpath = "//input[@class=\"_2yK7W3SWQ- _1d02bPcWht\"]")
    WebElement lowPriceRangeEditBox;

    @FindBy(xpath = "//input[@class=\"_2yK7W3SWQ- _1f2usTwyAs\"]")
    WebElement highPriceRangeEditBox;

    @FindBy(xpath = "//div[@class = \"n-snippet-cell2__title\"]//a[@class=\"link n-link_theme_blue link_type_cpc i-bem link_js_inited\"]")
    WebElement itemInList;

    @FindBy(xpath = "//div[@class = \"layout layout_type_search i-bem\"]//div[@class = \"preloadable__preloader preloadable__preloader_visibility_visible preloadable__paranja\"]")
    WebElement preloadParanja;


    public ItemsListPageMarket(WebDriver driver) {
        webDriver = driver;
        wait = new WebDriverWait(webDriver, 30, 500);
        PageFactory.initElements(webDriver, this);
    }

    public void clickCheckboxInCategoryOnFilterPanel(String nameCheckBox) {
        WebElement checkBox = webDriver.findElement(By.xpath("//div[@id=\"search-prepack\"]//span[normalize-space()=\"" + nameCheckBox + "\"]"));
        checkBox.click();
//        wait.until(ExpectedConditions.visibilityOf(preloadParanja));
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void setPriceFilter(String lowPrice, String highPrice) {
        lowPriceRangeEditBox.sendKeys(lowPrice);
        highPriceRangeEditBox.sendKeys(highPrice);
        wait.until(ExpectedConditions.urlContains(highPrice));
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public List<WebElement> getListOfItems() {
        List<WebElement> webElementList = webDriver.findElements(By.className("n-snippet-cell2__title"));
        return webElementList;
    }

    public void clickItemInListByTitle(String title) {
        WebElement specItemInList = webDriver.findElement(By.xpath("//div[@class=\"n-snippet-cell2__header\"]//a[@title=\"" + title + "\"]"));
        specItemInList.click();
    }
}
