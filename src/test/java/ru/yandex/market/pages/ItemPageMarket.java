package ru.yandex.market.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ItemPageMarket {

    private WebDriver webDriver;
    private WebDriverWait wait;

    @FindBy(className = "n-title__text")
    WebElement titleOnItemPage;

    public ItemPageMarket(WebDriver driver) {
        webDriver = driver;
        wait = new WebDriverWait(webDriver, 30, 500);
        PageFactory.initElements(webDriver, this);
    }

    public String getMainTitleText() {
        return titleOnItemPage.getText();
    }
}
