package ru.yandex.market.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MainPageMarket {
    private WebDriver webDriver;
    private WebDriverWait wait;


    @FindBy(xpath = "//div[@class = \"n-snippet-cell2__title\"]")
    WebElement itemInList;

    public MainPageMarket(WebDriver driver) {
        webDriver = driver;
        wait = new WebDriverWait(webDriver, 30, 500);
        PageFactory.initElements(webDriver, this);
    }

    public void switchSection(String nameSection) {
        WebElement specSectionButton = webDriver.findElement(By.xpath("//div[@class=\"n-w-tabs__horizontal-tabs-container\"]//a[normalize-space()=\"" + nameSection + "\"]"));
        specSectionButton.click();
    }

    public void chooseSubsection(String nameSubSection) {
        WebElement specSubSectionButton = webDriver.findElement(By.xpath("//div[@data-apiary-widget-name=\"@MarketNode/NavigationTree\"]//a[normalize-space()=\"" + nameSubSection + "\"]"));
        specSubSectionButton.click();
        wait.until(ExpectedConditions.visibilityOf(itemInList));
    }

}

