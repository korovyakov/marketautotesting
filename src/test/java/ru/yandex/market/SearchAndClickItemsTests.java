package ru.yandex.market;

import org.junit.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.site.MainSite;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class SearchAndClickItemsTests {

/*    Общий сценарий
    1.	Зайти на yandex.ru
    2.	Перейти в маркет, подраздел электроника.
    3.	Сделай фильтрацию по смартфонам марки Samsung.
    4.	Сделай фильтр по цене от 40 000 рублей.
    5.	Возьми имя первого смартфона, запомнить его.
    6.	Перейди по ссылке первого смартфона в его описание.
    7.	Сравни, что отображаемое имя телефона в описании совпадает с тем, которое ты запомнил.
    8.	Сделай все тоже самое п.3-7 с разделом наушники, марка beats, с ценой от 17 до 25 тыс. руб.*/


    WebDriver webDriver;
    MainSite webSite;
    WebDriverWait wait;

    @Before
    public void setUp() {

        webDriver = new ChromeDriver();
        wait = new WebDriverWait(webDriver, 30, 500);
        webSite = new MainSite(webDriver);

        webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS) ;
        webDriver.manage().timeouts().setScriptTimeout(30, TimeUnit.SECONDS);
        System.out.println("STEP #1.1: Зайти на yandex.ru");
        webDriver.get("https://yandex.ru/");
        webDriver.manage().window().maximize();
        Assert.assertTrue(webDriver.getCurrentUrl().contains("https://yandex.ru/"));

        System.out.println("STEP #1.2: Перейти в маркет, подраздел электроника.");
        webSite.mainPageSearch().chooseServiceFromMainBar("Маркет");
        webSite.mainPageMarket().switchSection("Электроника");
        Assert.assertTrue(webDriver.getCurrentUrl().contains("https://market.yandex.ru/catalog--elektronika/"));
    }

    @Test
    public void workWithMobilePhoneSearching() {

        String nameInFoundPhonesList, nameOnPhonePage;

        System.out.println("STEP #2.1: Сделай фильтрацию по смартфонам - марка Samsung");
        webSite.mainPageMarket().chooseSubsection("Мобильные телефоны");
        webSite.itemsListPageMarket().clickCheckboxInCategoryOnFilterPanel("Samsung");

        System.out.println("STEP #2.2: Сделай фильтр по цене от 40 000 рублей.");
        webSite.itemsListPageMarket().setPriceFilter("40000", "9999999");

        System.out.println("STEP #2.3: Возьми имя первого смартфона, запомнить его.");
        List<WebElement> foundPhones = webSite.itemsListPageMarket().getListOfItems();
        nameInFoundPhonesList = foundPhones.get(0).getText();

        System.out.println("STEP #2.4: Перейди по ссылке первого смартфона в его описание.");
        webSite.itemsListPageMarket().clickItemInListByTitle(nameInFoundPhonesList);

        System.out.println("STEP #2.5: Сравни, что отображаемое имя телефона в описании совпадает с тем, которое ты запомнил");
        nameOnPhonePage = webSite.itemPageMarket().getMainTitleText();
        Assert.assertTrue(nameInFoundPhonesList.equals(nameOnPhonePage));

    }

    @Test
    public void workWithHeadphonesSearching() {

        String nameInFoundHeadphonesList, nameOnHeadphonesPage;

        System.out.println("STEP #3.1: Сделай фильтрацию по наушникам - марка beats");
        webSite.mainPageMarket().chooseSubsection("Наушники и Bluetooth-гарнитуры");
        webSite.itemsListPageMarket().clickCheckboxInCategoryOnFilterPanel("Beats");

        System.out.println("STEP #3.2: Сделай фильтр по цене 17 до 25 тыс. руб.");
        webSite.itemsListPageMarket().setPriceFilter("17000", "25000");

        System.out.println("STEP #3.3: Возьми имя первых наушников, запомнить его.");
        List<WebElement> foundHeadphones = webSite.itemsListPageMarket().getListOfItems();
        nameInFoundHeadphonesList = foundHeadphones.get(0).getText();

        System.out.println("STEP #3.4: Перейди по ссылке первых найденных наушников в его описание.");
        webSite.itemsListPageMarket().clickItemInListByTitle(nameInFoundHeadphonesList);

        System.out.println("STEP #3.5: Сравни, что отображаемое имя наушников в описании совпадает с тем, которое ты запомнил");
        nameOnHeadphonesPage = webSite.itemPageMarket().getMainTitleText();
        Assert.assertTrue(nameInFoundHeadphonesList.equals(nameOnHeadphonesPage));


    }

    @After
    public void tearDown() {
        if (webDriver != null) {
            webDriver.quit();
        }
    }

}
