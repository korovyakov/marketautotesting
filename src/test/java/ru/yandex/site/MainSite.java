package ru.yandex.site;

import org.openqa.selenium.WebDriver;
import ru.yandex.market.pages.ItemPageMarket;
import ru.yandex.market.pages.ItemsListPageMarket;
import ru.yandex.market.pages.MainPageMarket;
import ru.yandex.pages.MainPageSearch;

public class MainSite {

    WebDriver webDriver;

    public MainSite(WebDriver driver) {
        webDriver = driver;
    }

    public MainPageSearch mainPageSearch() {
        return new MainPageSearch(webDriver);
    }

    public MainPageMarket mainPageMarket() {
        return new MainPageMarket(webDriver);
    }

    public ItemsListPageMarket itemsListPageMarket() {
        return new ItemsListPageMarket(webDriver);
    }

    public ItemPageMarket itemPageMarket() {
        return new ItemPageMarket(webDriver);
    }
}
