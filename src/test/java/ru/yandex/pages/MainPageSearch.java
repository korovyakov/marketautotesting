package ru.yandex.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MainPageSearch {
    private WebDriver webDriver;
    private WebDriverWait wait;


    public MainPageSearch(WebDriver driver) {
        webDriver = driver;
        wait = new WebDriverWait(webDriver, 30, 500);
        PageFactory.initElements(webDriver, this);
    }

    public void chooseServiceFromMainBar(String nameService) {
        WebElement serviceButton = webDriver.findElement(By.xpath("//div[@class=\"home-arrow__tabs\"]//a[normalize-space()=\"" + nameService + "\"]"));
        serviceButton.click();
    }
}
